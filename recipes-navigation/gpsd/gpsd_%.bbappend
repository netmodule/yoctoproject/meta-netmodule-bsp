
TTY_DEVICE = "/dev/gnss0"
USBAUTO_STATE = "false"

do_install_append () {
    sed -i 's|DEVICES=""|DEVICES="${TTY_DEVICE}"|g' ${D}/etc/default/gpsd.default
    sed -i 's|USBAUTO="true"|USBAUTO="${USBAUTO_STATE}"|g' ${D}/etc/default/gpsd.default 

    # gnss-mgr will force the speed to 115200
    if ${@bb.utils.contains('MACHINE_FEATURES', 'advanced-gnss', 'true', 'false', d)}; then
        sed -i "s/GPSD_OPTIONS=\"\(.*\)\"/GPSD_OPTIONS=\"\1 -s 115200\"/g" \
            ${D}${sysconfdir}/default/gpsd.default
    fi

}


# Service is started by gnss-mgr
SYSTEMD_AUTO_ENABLE = "${@bb.utils.contains("MACHINE_FEATURES", "advanced-gnss", "disable", "enable", d)}"
