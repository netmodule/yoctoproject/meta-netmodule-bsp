SUMMARY = "Start ti bluetooth"
DESCRIPTION = "Run hciattach for ti bluetooth wl12xx module"
AUTHOR = "Stefan Eichenberger"

SECTION = "connectivity"
LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"
PR = "r2"

inherit systemd
inherit allarch

RDEPENDS_${PN} = "\
    firmware-ti-wl18xx \
    bash \
    libgpiod-tools \
    "

SRC_URI =  " \
    file://tibluetooth.service \
    file://tibluetooth.sh \
    "

S = "${WORKDIR}"


SYSTEMD_SERVICE_${PN} = "tibluetooth.service"
SYSTEMD_AUTO_ENABLE ?= "enable"

FILES_${PN}_append = " \
                    ${nonarch_base_libdir} \
                    "

do_install () {
    sed -i 's:BINDIR:${bindir}:g' tibluetooth.service
    install -d ${D}${systemd_unitdir}/system/
    install -m 0644 tibluetooth.service ${D}${systemd_unitdir}/system/tibluetooth.service
    install -d ${D}${bindir}
    install -m 0755 tibluetooth.sh ${D}${bindir}/tibluetooth
}

