#!/usr/bin/env bash

PIDFILE_GPIO=/run/bluetooth0-gpio.pid
PIDFILE_HCI=/run/bluetooth0-hci.pid

enable_bt_chip() {
        BT_EN=$(gpiofind BT_EN)
        gpioset -m signal ${BT_EN}=1 &
        PID_GPIO=$!
        echo $PID_GPIO > $PIDFILE_GPIO
}

disable_bt_chip() {
        kill $(<$PIDFILE_GPIO)
}

if [ "$1" = "start" ]; then
        retries=0
        while [ $retries -lt 5 ]
        do
                enable_bt_chip
                if HCI_LOG=$(hciattach -p /dev/bluetooth0 texas 3000000 flow ); then
                        # Writing PID
                        echo "$HCI_LOG" | tail -n 1 > $PIDFILE_HCI
                        exit 0
                fi

                echo "$HCI_LOG"
                disable_bt_chip
                ((retries++))
                sleep 1
        done

        echo "Could not start hciattach"
        exit -1

elif [ "$1" = "stop" ]; then
        kill $(<$PIDFILE_HCI)
        disable_bt_chip
else
        echo "Unknown command"
        exit -1
fi
