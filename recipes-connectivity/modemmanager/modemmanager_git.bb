require recipes-connectivity/modemmanager/modemmanager_1.14.8.bb

# Use custom git repo as SRC
SRC_URI = "git://gitlab.com/netmodule/third-party/ModemManager;protocol=ssh;user=git;branch=mm-1-14-netmodule;"
SRCREV ?= "35ef2f387bf53f0601901a5f08ab0f6bf57105c4"
S = "${WORKDIR}/git"

# Keep only ublox plugin
EXTRA_OECONF += "--disable-all-plugins --enable-plugin-ublox --enable-plugin-generic"

# Exclude mbim
PACKAGECONFIG = "systemd qmi"

# Add whitelist rules
FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
SRC_URI += "file://77-mm-netmodule-whitelist.rules"

do_install_append() {
    install -m 0644 ${WORKDIR}/77-mm-netmodule-whitelist.rules ${D}${libdir}/udev/rules.d/
    sed -i -e 's/bin\/ModemManager/bin\/ModemManager --filter-policy=WHITELIST-ONLY/g' ${D}${systemd_unitdir}/system/ModemManager.service
}

# MM is started by wwan-config
SYSTEMD_AUTO_ENABLE = "disable"
