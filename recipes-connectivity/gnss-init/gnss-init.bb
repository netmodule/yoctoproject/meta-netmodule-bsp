# Copyright (C) 2019 Ramon Moesching <ramon.moesching@netmodule.com>
# Released under the MIT license (see COPYING.MIT for the terms)
DESCRIPTION = "GNSS init service"
HOMEPAGE = "www.netmodule.com"
LICENSE = "MIT"
SECTION = "bsp/firmware"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"


inherit systemd

FILESEXTRAPATHS_append := ":${THISDIR}/files"

SRC_URI_append = " \ 
           file://gnss-init.service \
           "

S = "${WORKDIR}"

SYSTEMD_SERVICE_${PN} = " \
                          gnss-init.service \
                        "

FILES_${PN}_append = "${systemd_unitdir}/system ${bindir}"

do_install_append() {
    install -d ${D}${systemd_unitdir}/system
    install -m 644 ${WORKDIR}/gnss-init.service ${D}${systemd_unitdir}/system/
}
