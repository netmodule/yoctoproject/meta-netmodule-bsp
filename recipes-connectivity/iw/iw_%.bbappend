# referes to meta-netmodule-wlan layer
DEPENDS = "libnl"

CFLAGS = " "
LDFLAGS = " "

EXTRA_OEMAKE = "\
    -f '${S}/Makefile' \
    \
    'PREFIX=${prefix}' \
    'SBINDIR=${sbindir}' \
    'MANDIR=${mandir}' \
"

CFLAGS += "-lnl-genl"
