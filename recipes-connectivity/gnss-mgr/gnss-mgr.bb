DESCRIPTION = "NetModule GNSS management tool"
LICENSE = "LGPLv3"
LIC_FILES_CHKSUM = "file://LICENSE;md5=3000208d539ec061b899bce1d9ce9404"

RDEPENDS_${PN} += " \
    python3-setuptools \
    nmubxlib \
    python3-systemd \
    "

DEPENDS = "python3-setuptools-git-version-native"

inherit setuptools3
inherit gitpkgv

inherit systemd
SYSTEMD_SERVICE_${PN} = "gnss-mgr.service"

inherit allarch

# Package Version (built from tags)
PKGV = "${GITPKGVTAG}"
# Recipe Version
PV = "1.0-git${SRCPV}"
PR = "r1"

SRC_URI = "git://gitlab.com/netmodule/tools/gnssmgr.git;protocol=ssh;user=git;branch=master"
SRCREV = "a8e4d440af630790d62aed59f0e2302749adf27c"
S = "${WORKDIR}/git"

PACKAGES =+ "${PN}-test"
SUMMARY_${PN}-test = "Addon to gnss-mgr for testing purposes"
FILES_${PN}-test = "${bindir}/gnss-config-reader"

do_install_append() {
    install -d ${D}${sysconfdir}/gnss
    install -m 0644 ${S}/config/gnss-neom8.conf ${D}${sysconfdir}/gnss/gnss0.conf

    install -d ${D}/${sbindir}/
    install -m 0755 ${S}/systemd/gnss-mgr-service.py ${D}${sbindir}/gnss-mgr-service

    install -d ${D}/${systemd_unitdir}/system/
    install -m 0644 ${S}/systemd/gnss-mgr.service ${D}/${systemd_unitdir}/system/

    # Test tool
    install -d ${D}${bindir}/
    install -m 0755 ${S}/testing/gnss-config-reader.py ${D}${bindir}/gnss-config-reader
}
