DESCRIPTION = "Library implementing UBX protocol for u-blox gnss modems"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=331a9220a74b05f133bf1ef09b6903b2"

RDEPENDS_${PN} += " \
    python3-setuptools \
    "

DEPENDS = "python3-setuptools-git-version-native"

inherit setuptools3
inherit gitpkgv

# Package Version (built from tags)
PKGV = "${GITPKGVTAG}"
# Recipe Version
PV = "1.0-git${SRCPV}"
PR = "r1"

SRC_URI = "git://gitlab.com/netmodule/tools/nmubxlib.git;protocol=ssh;user=git;branch=master"
SRCREV = "7c22b57cdd169a6651cb0c17a215d1e448727bcb"
S = "${WORKDIR}/git"


