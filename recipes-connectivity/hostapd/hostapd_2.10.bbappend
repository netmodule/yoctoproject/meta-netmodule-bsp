FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

DEPENDS = "libnl dbus"
 
export EXTRA_CFLAGS = " \
     ${CFLAGS} \
    -I${STAGING_DIR_TARGET}/usr/include/libnl3 \
"

do_configure_prepend () {
    sed -i 's/CONFIG_LIBNL_TINY=y/#CONFIG_LIBNL_TINY=y/g' ${WORKDIR}/hostapd-full.config
}
