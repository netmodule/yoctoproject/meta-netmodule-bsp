DEPENDS = "libnl dbus"

export EXTRA_CFLAGS = " \
     ${CFLAGS} \
    -I${STAGING_DIR_TARGET}/usr/include/libnl3 \
"

SYSTEMD_SERVICE_${PN} = "wpa_supplicant.service wpa_supplicant-nl80211@.service wpa_supplicant-wired@.service"
SYSTEMD_AUTO_ENABLE = "disable"

FILES_${PN} += "/usr/lib/systemd/system/*"

do_configure_prepend () {
    sed -i 's/#CONFIG_CTRL_IFACE_DBUS_NEW=y/CONFIG_CTRL_IFACE_DBUS_NEW=y/g' ${WORKDIR}/wpa_supplicant-full.config
    sed -i 's/#CONFIG_CTRL_IFACE_DBUS_INTRO=y/CONFIG_CTRL_IFACE_DBUS_INTRO=y/g' ${WORKDIR}/wpa_supplicant-full.config

    sed -i 's/CONFIG_LIBNL_TINY=y/#CONFIG_LIBNL_TINY=y/g' ${WORKDIR}/wpa_supplicant-full.config
}


do_install_append () {

	install -d ${D}/${sysconfdir}/dbus-1/system.d
	install -m 644 ${S}/wpa_supplicant/dbus/dbus-wpa_supplicant.conf ${D}/${sysconfdir}/dbus-1/system.d
	install -d ${D}/${datadir}/dbus-1/system-services
	install -m 644 ${S}/wpa_supplicant/dbus/*.service ${D}/${datadir}/dbus-1/system-services

	if ${@bb.utils.contains('DISTRO_FEATURES','systemd','true','false',d)}; then
		install -d ${D}/${systemd_unitdir}/system
		install -m 644 ${S}/wpa_supplicant/systemd/*.service ${D}/${systemd_unitdir}/system
	fi
}
