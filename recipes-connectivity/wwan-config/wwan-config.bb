DESCRIPTION = "WWAN modems configuration tool"
LICENSE = "Proprietary"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/Proprietary;md5=0557f9d92cf58f2ccdd50f62f8ac0b28"

RDEPENDS_${PN} += " \
    python3-pyserial \
    libgpiod-python \
    python3-setuptools \
    python3-configparser \
    python3-systemd \
    lmsensors-sensors \
    lmsensors-config-libsensors \
    "
DEPENDS = "python3-setuptools-git-version-native"

inherit gitpkgv systemd allarch


# Package Version (built from tags)
PKGV = "${GITPKGVTAG}"
# Recipe Version
PV = "1.0-git${SRCPV}"

PR = "r1"

SRCREV = "a943fa76d5385c3f9bc54023c6fcf8b7bf339fd1"
SRC_URI = "git://gitlab.com/netmodule/tools/wwan-config.git;protocol=ssh;user=git;branch=master \
           file://wwan-config@.service \
           file://default.conf \
          "
S = "${WORKDIR}/git"

inherit setuptools3 

do_install_append() {
    install -d ${D}/${systemd_unitdir}/system/
    install -m 0644 ${WORKDIR}/wwan-config@.service ${D}/${systemd_unitdir}/system/

    install -d ${D}/${sysconfdir}/wwan
    if [ ! -z "${WWAN_NBR}" ] ; then
        install -d ${D}${sysconfdir}/systemd/system/multi-user.target.requires/
        for i in `seq 0 ${WWAN_NBR}`; do 
            if [ $i = ${WWAN_NBR} ]; then continue; fi

            if [ "${SYSTEMD_AUTO_ENABLE}" = "enable" ] ; then
                ln -sf ${systemd_unitdir}/system/wwan-config@.service \
                    ${D}${sysconfdir}/systemd/system/multi-user.target.requires/wwan-config@wwan$i.service
            fi

            install -m 0644 ${WORKDIR}/default.conf ${D}${sysconfdir}/wwan/wwan$i.conf
            if [ ! -z "${NM_WWAN_APN}" ]; then
                sed -i 's/apn=/apn=${NM_WWAN_APN}/g' ${D}${sysconfdir}/wwan/wwan$i.conf
            fi
            if [ ! -z "${NM_WWAN_USER}" ]; then
                sed -i 's/user=/user=${NM_WWAN_USER}/g' ${D}${sysconfdir}/wwan/wwan$i.conf
            fi
            if [ ! -z "${NM_WWAN_PASSWORD}" ]; then
                sed -i 's/password=/password=${NM_WWAN_PASSWORD}/g' ${D}${sysconfdir}/wwan/wwan$i.conf
            fi
        done
    fi
}

