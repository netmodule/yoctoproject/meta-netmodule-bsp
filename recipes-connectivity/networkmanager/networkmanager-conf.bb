DESCRIPTION = "Configuration files for NetworkManager"

LICENSE = "Proprietary"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/Proprietary;md5=0557f9d92cf58f2ccdd50f62f8ac0b28"

RDEPENDS_${PN} = "networkmanager"
PACKAGECONFIG ?= ""

export DEFAULT_ETH ??= "eth0"
PACKAGECONFIG[unmanaged-devices] = ""
PACKAGECONFIG[ethernet-dhcp] = ""

SRC_URI = " \
                 file://eth0-static \
                 file://eth0-dhcp \
                 file://bridge-slave-eth0.nmconnection \
                 file://NetworkManager.conf \
                 file://00-fallback-dns.conf \
                 file://unmanaged-devices.conf \
"

SRC_URI_append_am335x-nrhw20 = " \
                file://system-connections-static.tar.gz \
                file://system-connections-dhcp.tar.gz \
"

PACKAGE_ARCH = "${MACHINE_ARCH}"

SYSTEM_CONNECTIONS = "${D}${sysconfdir}/NetworkManager/system-connections/"

do_install () {
    install -d ${D}${sysconfdir}/NetworkManager/dnsmasq.d
    install -d ${SYSTEM_CONNECTIONS}
    install -m 0644 ${WORKDIR}/NetworkManager.conf ${D}${sysconfdir}/NetworkManager/
    install -m 0644 ${WORKDIR}/00-fallback-dns.conf ${D}${sysconfdir}/NetworkManager/dnsmasq.d/

    static_ifaces=""
    dhcp_ifaces=""
    bridged_ifaces=""

    if ${@bb.utils.contains('PACKAGECONFIG','ethernet-dhcp','true','false',d)}; then
        # DHCP on main interface and static IP on the user module interface (if present)
        dhcp_ifaces="$DEFAULT_ETH"
        if ${@bb.utils.contains('MACHINE_FEATURES','user-module','true','false',d)}; then
            static_ifaces="umnet0"
        fi
    else
        # static IP either on the bridge or main interface
        if ${@bb.utils.contains('MACHINE_FEATURES','user-module','true','false',d)}; then
            static_ifaces="br0"
            bridged_ifaces="$DEFAULT_ETH umnet0"
        else
            static_ifaces="$DEFAULT_ETH"
        fi
    fi


    for iface in $static_ifaces; do
        install -m 0600 ${WORKDIR}/eth0-static ${SYSTEM_CONNECTIONS}/$iface.nmconnection
        sed -i "s/eth0/$iface/g" ${SYSTEM_CONNECTIONS}/$iface.nmconnection

        # Handle bridges
        if ! [ "$iface" = "${iface#br}" ]; then
            # Change type of main connection to bridge
            sed -i "s/type=.*/type=bridge/g" ${SYSTEM_CONNECTIONS}/$iface.nmconnection
            printf "\n[bridge]\nstp=false\n" >> ${SYSTEM_CONNECTIONS}/$iface.nmconnection
        fi
    done

    for iface in $dhcp_ifaces; do
        install -m 0600 ${WORKDIR}/eth0-dhcp ${SYSTEM_CONNECTIONS}/$iface.nmconnection
        sed -i "s/eth0/$iface/g" ${SYSTEM_CONNECTIONS}/$iface.nmconnection
    done


    for iface in $bridged_ifaces; do
        install -m 0600 ${WORKDIR}/bridge-slave-eth0.nmconnection ${SYSTEM_CONNECTIONS}/$iface.nmconnection
        sed -i "s/eth0/$iface/g" ${SYSTEM_CONNECTIONS}/$iface.nmconnection
    done


    if ${@bb.utils.contains('PACKAGECONFIG','unmanaged-devices','true','false',d)}; then
        install -d ${D}${sysconfdir}/NetworkManager/conf.d
        install -m 0644 ${WORKDIR}/unmanaged-devices.conf ${D}${sysconfdir}/NetworkManager/conf.d/
    fi
}

do_install_append_am335x-nrhw20() {
    rm -rf ${SYSTEM_CONNECTIONS}/*
    if ${@bb.utils.contains('PACKAGECONFIG','ethernet-dhcp','true','false',d)}; then
        install -m 0600 ${WORKDIR}/system-connections-dhcp/* ${SYSTEM_CONNECTIONS}/
    else
        install -m 0600 ${WORKDIR}/system-connections-static/* ${SYSTEM_CONNECTIONS}/
    fi
}


FILES_${PN} = " \
    ${sysconfdir}/NetworkManager/NetworkManager.conf \
    ${sysconfdir}/NetworkManager/dnsmasq.d \
    ${sysconfdir}/NetworkManager/system-connections \
    ${sysconfdir}/NetworkManager/conf.d \
"


