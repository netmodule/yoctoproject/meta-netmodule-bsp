FILESEXTRAPATHS_append := "${THISDIR}/${PN}"

RDEPENDS_${PN} = "networkmanager-conf"

SRC_URI_append = "\
                 file://0001-Reactivate-GSM-connections-when-ModemManager-reconne.patch \
                 file://0001-wwan-Set-MTU-based-on-what-ModemManager-exposes.patch \
                 "

PACKAGECONFIG[dnsmasq] = "--with-dnsmasq=${bindir}/dnsmasq,,,dnsmasq"
