do_assemble_fitimage[depends] += "fpga-image:do_deploy"

#
# Emit the fitImage ITS fpga section
#
# $1 ... .its filename
# $2 ... Image counter
# $3 ... Path to fpga image
fitimage_emit_section_kernel_append() {
        fpgacount=1
        cat << EOF >> ${1}
                fpga@${fpgacount} {
                        description = "FPGA";
                        data = /incbin/("${DEPLOY_DIR_IMAGE}/fpga-image-${MACHINE}");
                        type = "fpga";
                        arch = "${UBOOT_ARCH}";
                        compression = "none";
                        hash@1 {
                                algo = "sha1";
                        };
                };
EOF
}
