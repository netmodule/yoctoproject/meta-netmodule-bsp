# Copyright (C) 2021 Netmodule AG
DESCRIPTION = "implements a publish/subscribe broker for the various system state topics"
HOMEPAGE = "http://www.netmodule.com/"
LICENSE = "GPL-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"   

inherit packagegroup

PROVIDES = "${PN}"

RDEPENDS_${PN} = " \
    ssc-broker-driver \
    ssc-sysstate-driver \
    "
