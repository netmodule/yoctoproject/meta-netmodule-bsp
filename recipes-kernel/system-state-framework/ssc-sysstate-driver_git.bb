# Copyright (C) 2021 Netmodule AG

DESCRIPTION = "implements a publish/subscribe broker for the various system state topics"
HOMEPAGE = "http://www.netmodule.com/"
LICENSE = "GPL-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"

DEPENDS = "ssc-broker-driver"
RDEPENDS_${PN} = "ssc-broker-driver"

inherit module

KERNEL_MODULE_AUTOLOAD += "sysstate"

# We are building one kernel for all machines with the same architecture
PACKAGE_ARCH = "${TUNE_PKGARCH}"

PV = "1.0.0"
SRCREV ?= "e6cd905b06a04af2a1a5fedd07d1ce0c34cfe433"

EXTRA_OEMAKE += "ccflags-y=\"-I${STAGING_INCDIR}/\""

SRC_URI = "\
    git://gitlab.com/netmodule/kernel/ssc-sysstate-driver.git;protocol=ssh;user=git;branch=develop \
    "

S = "${WORKDIR}/git"

python __anonymous () {
    depends = d.getVar('DEPENDS')
    extra_symbols = []
    for dep in depends.split():
        if dep.startswith("kernel-module-"):
            extra_symbols.append("${STAGING_INCDIR}/" + dep + "/Module.symvers")
    extra_symbols.append("${STAGING_INCDIR}/ssc-broker-driver/Module.symvers")
    d.setVar('KBUILD_EXTRA_SYMBOLS', " ".join(extra_symbols))
}
