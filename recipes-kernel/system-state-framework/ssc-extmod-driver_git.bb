# Copyright (C) 2021 Netmodule AG

DESCRIPTION = "implements a publish/subscribe broker for the various system state topics"
HOMEPAGE = "http://www.netmodule.com/"
LICENSE = "GPL-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"

DEPENDS = "ssc-broker-driver"
RDEPENDS_${PN} = "ssc-broker-driver"

inherit module

KERNEL_MODULE_AUTOLOAD += "extmod"

# We are building one kernel for all machines with the same architecture
PACKAGE_ARCH = "${TUNE_PKGARCH}"

PV = "1.0.0"
SRCREV ?= "916fb314511674363cf3a8f6d0bccc092cb00efc"

EXTRA_OEMAKE += "ccflags-y=\"-I${STAGING_INCDIR}/\""

SRC_URI = " \
    git://gitlab.com/netmodule/tools/ssc-extmod-driver.git;protocol=ssh;user=git;branch=develop \
    "    

S = "${WORKDIR}/git"
