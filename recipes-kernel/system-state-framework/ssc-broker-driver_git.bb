# Copyright (C) 2021 Netmodule AG

DESCRIPTION = "implements a publish/subscribe broker for the various system state topics"
HOMEPAGE = "http://www.netmodule.com/"
LICENSE = "GPL-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"

inherit module

KERNEL_MODULE_AUTOLOAD += "broker"

# We are building one kernel for all machines with the same architecture
PACKAGE_ARCH = "${TUNE_PKGARCH}"

PV = "1.0.0"
SRCREV ?= "4950e67e4a94bde44f8cf7a8c3f0c8eed8d18ad9"

SRC_URI = "git://gitlab.com/netmodule/kernel/ssc-broker-driver.git;protocol=ssh;user=git;branch=develop"


do_install_append () {
    install -d ${D}${includedir}/broker
    install -m 0644 ${S}/broker.h ${D}${includedir}/broker/broker.h
    install -m 0644 ${S}/worker.h ${D}${includedir}/broker/worker.h
}

S = "${WORKDIR}/git"
