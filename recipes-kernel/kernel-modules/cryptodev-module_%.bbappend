# We are building one kernel for all machines with the same architecture
PACKAGE_ARCH = "${TUNE_PKGARCH}"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"
SRC_URI:append = " file://0002-Fix-build-for-linux-5.10.220.patch"
