require linux-netmodule.inc

SRC_URI = "git://gitlab.com/netmodule/kernel/linux-netmodule.git;protocol=ssh;user=git;branch=5.10/standard/base"
SRCREV = "2672b3c4bab4a5d533784a1c4fd56004c2c51bd2"

# Config fragments
FILESEXTRAPATHS_prepend := "${THISDIR}/conf:"
SRC_URI_append_netbird = " file://wlan.cfg"
