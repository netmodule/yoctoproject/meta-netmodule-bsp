# Copyright (C) 2021 Lucien Mueller <lucien.mueller@netmodule.com>

DESCRIPTION = "make fitimage"
LICENSE = "GPLv2"


inherit kernel-fitimage
inherit ${@bb.utils.contains('MACHINE_FEATURES', 'fpga', 'kernel-fitimage-fpga', '', d)}
inherit kernel-arch
inherit deploy

do_configure[depends] += "virtual/kernel:do_deploy"
do_configure[depends] += "${@bb.utils.contains('MACHINE_FEATURES', 'fpga', 'fpga-image:do_deploy', '', d)}"
do_bundle_initramfs[depends] += "${INITRAMFS_IMAGE}:do_image_complete"
do_deploy[depends] += "${PN}:do_assemble_fitimage"

KERNEL_IMAGETYPES = "fitImage"
PACKAGE_ARCH = "${MACHINE_ARCH}"

PROVIDES = "virtual/netmodule-image"
PROVIDES_sota = ""

addtask assemble_fitimage_initramfs before do_install after do_compile
addtask do_deploy after assemble_fitimage_initramfs

INITRAMFS_IMAGE_NAME ?= "${INITRAMFS_IMAGE}-${MACHINE}"

UBOOT_LOADADDRESS ?= "${UBOOT_ENTRYPOINT}"

KERNEL_DEVICETREE = "${KERNEL_DEVICETREE_TO_USE}"

do_configure_prepend () {
	install -d ${S}/arch/${ARCH}/boot/
	for DTB in ${KERNEL_DEVICETREE}; do
		# We have to watch out for when the device-tree is located in its own subdirectory in the kernel sources.
		# DTB_SUBDIR will contain this directory.
		DTB_DEPLOY=`basename ${DTB}`
		DTB_SUBDIR=`dirname ${DTB}`
		install -d arch/${ARCH}/boot/${DTB_SUBDIR}
		install -m 0644 ${DEPLOY_DIR_IMAGE}/${DTB_DEPLOY} arch/${ARCH}/boot/${DTB}
	done
	install -m 0644 ${DEPLOY_DIR_IMAGE}/${KERNEL_IMAGETYPE}-${KERNEL_IMAGE_LINK_NAME}.bin arch/${ARCH}/boot/vmlinuz.bin
}

kernel_do_deploy () {
	# The kernel-fitimage class appends the deployment to this task. We just have to provide the task.
}

do_deploy () {
	deployDir=${DEPLOYDIR}
	kernel_do_deploy
}




