# Copyright (C) 2018 NetModule AG

inherit kernel

DESCRIPTION = "Linux kernel for various NetModule hardware"

LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

# Avoids installing the kernel in /boot directory
RDEPENDS_${KERNEL_PACKAGE_NAME}-base = ""

# We are building one kernel for all machines with the same architecture
PACKAGE_ARCH = "${TUNE_PKGARCH}"

# This variable is using MACHINE by default. Since we don't use it,
# overwrite with dummy value not using MACHINE variable
INITRAMFS_IMAGE_NAME = "-"

S = "${WORKDIR}/git"

INSANE_SKIP_${PN} += "version-going-backwards"

copy_initramfs () {
    # We do not use this feature and it is depending on MACHINE variable
    # So to avoid this dependency we overwrite it with an empty function
    true
}

#If a KERNEL_DEFCONFIG is specified, the defconfig specified in SRC_URI will be overwritten!
do_configure_append(){
    if [ "${KERNEL_DEFCONFIG}" != "" ]; then
        oe_runmake ${KERNEL_DEFCONFIG}
    fi

    configs="${@" ".join(find_cfgs(d))}"
    if [ ! -z "${configs}" ]; then
        ${S}/scripts/kconfig/merge_config.sh -m -O ${WORKDIR}/build ${WORKDIR}/build/.config ${WORKDIR}/*.cfg
    fi
}
