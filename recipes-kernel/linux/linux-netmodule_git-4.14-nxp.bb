require linux-netmodule.inc

SRC_URI = "git://gitlab.com/netmodule/kernel/linux-netmodule.git;protocol=ssh;user=git;branch=4.14/nxp/nmhw23"
SRCREV ?= "3ee53ea1ed03e93237e594621490839a0e2052d6"
LIC_FILES_CHKSUM = "file://COPYING;md5=d7810fab7487fb0aad327b76f1be7cd7"
