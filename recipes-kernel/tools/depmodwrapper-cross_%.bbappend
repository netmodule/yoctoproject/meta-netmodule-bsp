# This tool is just a wrapper for the kernel build in yocto
# Since we build the kernel for each architecture and not for each machine,
# We also need to build this recipe for each architecture and not for all machine

PACKAGE_ARCH = "${TUNE_PKGARCH}"
