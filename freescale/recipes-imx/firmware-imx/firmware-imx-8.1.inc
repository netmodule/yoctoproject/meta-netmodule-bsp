# Copyright (C) 2012-2016 Freescale Semiconductor
# Copyright 2017-2018 NXP
# Copyright (C) 2018 O.S. Systems Software LTDA.
SECTION = "base"
LICENSE = "Proprietary"
LIC_FILES_CHKSUM = "file://COPYING;md5=80c0478f4339af024519b3723023fe28"

SRCBRANCH ?= "master"
SRC_URI = " \
    ${FSL_MIRROR}/firmware-imx-${PV}.bin;fsl-eula=true \
"

#SRC_URI[md5sum] = "0967aa59b3fd8d80fcb98146a9aac91b"
#SRC_URI[sha256sum] = "910fbf866f61185adfd60c1704b2da41030cb175901d06e40402b49f9240bdee"

SRC_URI[md5sum] = "ff7e208761379890261b62f477b441ed"
SRC_URI[sha256sum] = "d6a1d8dc3ce8f2e928bc6b58c7d583126abfd14d8ab61a2d8ebd760a898b5195"


S = "${WORKDIR}/firmware-imx-${PV}"

inherit fsl-eula-unpack
