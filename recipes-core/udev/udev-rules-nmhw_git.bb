# Copyright (C) 2019 Ramon Moesching <ramon.moesching@netmodule.com>
# Released under the MIT license (see COPYING.MIT for the terms)

DESCRIPTION = "udev rules for netmodule router and oem hardware"
HOMEPAGE = "netmodule.com"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SECTION = "base"

S = "${WORKDIR}/git"
SRCREV = "5614dee49096f017dffb7376de4b71f9d6d1f8dc"

SRC_URI = "git://gitlab.com/netmodule/tools/udev-rules-nmhw;protocol=ssh;user=git;branch=master;"

do_install () {
    install -d ${D}${sysconfdir}/udev/rules.d

    if ls ${S}/machines/${MACHINE}/*.rules 1> /dev/null 2>&1 ; then
       install -m 0644 ${S}/machines/${MACHINE}/*.rules ${D}${sysconfdir}/udev/rules.d/
       if [ -f ${S}/machines/${MACHINE}/*.sh ]; then
           install -m 0755 ${S}/machines/${MACHINE}/*.sh ${D}${sysconfdir}/udev/rules.d/
       fi
    fi
}
