SOC_FAMILY = "ti33x"
require conf/machine/include/soc-family.inc
require netmodule-hardware.inc

DEFAULTTUNE ?= "cortexa8thf-neon"
require conf/machine/include/tune-cortexa8.inc

PREFERRED_PROVIDER_virtual/xserver = "xserver-xorg"

# For built-in LCD, add xf86-input-tslib
XSERVER = "xserver-xorg \
           xf86-input-evdev \
           xf86-input-mouse \
           xf86-video-fbdev \
           xf86-input-keyboard"

# Default to external video, change to smallscreen for built-in LCD
GUI_MACHINE_CLASS = "bigscreen"

# Increase this everytime you change something in the kernel
MACHINE_KERNEL_PR = "r22"

# Default providers, may need to override for specific machines
PREFERRED_PROVIDER_virtual/kernel ?= "linux-netmodule"
PREFERRED_VERSION_linux-netmodule ?= "git-5.10"
PREFERRED_PROVIDER_virtual/bootloader ?= "u-boot-ti-staging"
PREFERRED_PROVIDER_u-boot ?= "u-boot-ti-staging"

KERNEL_IMAGETYPE = "uImage"

UBOOT_ARCH = "arm"
UBOOT_MACHINE ?= "am335x_evm_config"

UBOOT_ENTRYPOINT ?= "0x80008000"
UBOOT_LOADADDRESS ?= "0x80008000"
SOTA_MACHINE = "am335x"
IMAGE_BOOT_FILES = "MLO u-boot.img"

# Use the expected value of the ubifs filesystem's volume name in the kernel
# and u-boot.
UBI_VOLNAME = "rootfs"

# List common SoC features, may need to add touchscreen for specific machines
MACHINE_FEATURES = "kernel26 apm usbgadget usbhost pci vfat ext2 screen alsa ethernet sgx"


NM_TARGET = "netbird"
NM_ARCH = "arm"
MACHINEOVERRIDES =. "${NM_TARGET}:"


# We have one kernel for all TI machines
KERNEL_IMAGETYPE = "zImage"
KERNEL_DEFCONFIG = "am335x-netmodule_defconfig"

# We need to specify all devicetrees here in order to build them with the kernel
HW16_DT = "am335x-nrhw16.dtb \
    am335x-nrhw16-prod2.dtb \
    am335x-nrhw16-prod3.dtb \
    am335x-nrhw16-prod4.dtb \
    am335x-nrhw16-prod5.dtb \
    "
HW20_DT = "am335x-nrhw20-prod1.dtb"
HW21_DT = "am335x-nmhw21-prod1.dtb"
HW24_DT = "am335x-nmhw24-prod1.dtb"
HW25_DT = "am335x-hw25.dtb"
HW26_DT = "am335x-hw26.dtb"

KERNEL_DEVICETREE = "${HW16_DT} ${HW20_DT} ${HW21_DT} ${HW24_DT} ${HW25_DT} ${HW26_DT}"
