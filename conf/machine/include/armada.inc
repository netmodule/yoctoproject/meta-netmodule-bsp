SOC_FAMILY = "armada"
require conf/machine/include/soc-family.inc
require netmodule-hardware.inc

DEFAULTTUNE ?= "cortexa9thf-neon"
require conf/machine/include/tune-cortexa9.inc

# Default providers, may need to override for specific machines
PREFERRED_PROVIDER_virtual/bootloader = "u-boot-armada"
PREFERRED_PROVIDER_u-boot = "u-boot-armada"

UBOOT_ARCH = "arm"
UBOOT_MACHINE = "mvebu_db_armada8k_config"

UBOOT_ENTRYPOINT = "0x13000000"
UBOOT_LOADADDRESS = "0x13000000"

# List common SoC features, may need to add touchscreen for specific machines
MACHINE_FEATURES = "kernel26 usbgadget usbhost pci vfat ext2 ext4 ethernet"

SOTA_MACHINE = "armada-385"

NM_TARGET = "netbolt"
NM_ARCH = "arm"
MACHINEOVERRIDES =. "${NM_TARGET}:"

PREFERRED_PROVIDER_virtual/kernel ?= "linux-netmodule"
PREFERRED_VERSION_linux-netmodule ?= "git-5.10"

# We have one kernel for all armada machines
KERNEL_IMAGETYPE = "zImage"
KERNEL_DEFCONFIG = "armada-385-netmodule_defconfig"

HW17_DT = "armada-385-nbhw17-prod1.dtb"
HW18_DT = "armada-385-nrhw18-prod1.dtb"

KERNEL_DEVICETREE = "${HW17_DT} ${HW18_DT}"
