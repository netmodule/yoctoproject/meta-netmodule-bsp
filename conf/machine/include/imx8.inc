require conf/machine/include/imx-base.inc
require netmodule-hardware.inc

# Prevent the use of imx mainline BSP
IMX_DEFAULT_BSP = ""

# Prevent usage of dynamic-packagearch for the kernel
MACHINE_ARCH_FILTER = ""

require conf/machine/include/tune-cortexa35.inc

MACHINEOVERRIDES =. "mx8:mx8x:mx8qxp:"

KERNEL_IMAGETYPE = "Image"
KERNEL_ALT_IMAGETYPE = "Image"
UBOOT_ENTRYPOINT = "0x80280000"

PREFERRED_PROVIDER_virtual/kernel ?= "linux-netmodule"
PREFERRED_VERSION_linux-netmodule ?= "git-4.14-nxp"
KERNEL_DEFCONFIG = "imx8-netmodule_defconfig"

HW23_DT = "netmodule/imx8-nmhw23.dtb"
HW23_DLM_DT = "netmodule/imx8-nmhw23-dlm.dtb"
KERNEL_DEVICETREE = "${HW23_DT} ${HW23_DLM_DT}"
