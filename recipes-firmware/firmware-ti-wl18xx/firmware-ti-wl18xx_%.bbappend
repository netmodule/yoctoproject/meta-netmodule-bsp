FILESEXTRAPATHS_append:= ":${THISDIR}/firmware-ti-wl18xx"

SRC_URI_append = " file://wl1271-nvs.bin "


do_install_append() {
        install -m 0644 ${WORKDIR}/wl1271-nvs.bin ${D}${nonarch_base_libdir}/firmware/ti-connectivity/wl1271-nvs.bin
}
