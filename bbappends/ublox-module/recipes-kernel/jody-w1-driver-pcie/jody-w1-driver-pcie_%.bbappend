FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

RDEPENDS_${PN}_remove = " ${PN}-wpa-supplicant ${PN}-hostapd"

SRC_URI_append = "file://0001-Disable-wowlan_config.patch"

# unblacklist pcie driver and fw files (default)
do_configure_prepend () {
    for i in $(seq 1 4); do
        sed -e "${i}s/^#*/#/" -i ${WORKDIR}/jody-w1-driver-pcie.conf
    done
}
