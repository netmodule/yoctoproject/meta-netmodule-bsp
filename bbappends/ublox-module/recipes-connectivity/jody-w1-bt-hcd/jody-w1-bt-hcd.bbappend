# Copyright (C) 2019 Ramon Moesching <ramon.moesching@netmodule.com>
# Released under the MIT license (see COPYING.MIT for the terms)

inherit systemd

RDEPENDS_${PN} += "libgpiod-tools"

FILESEXTRAPATHS_append := ":${THISDIR}/files"

SRC_URI_append = " \ 
           file://jody-w1-bt-init.service \
           file://jody-w1-bt \
           "

S = "${WORKDIR}"

SYSTEMD_SERVICE_${PN} = " \
                          jody-w1-bt-init.service \
                        "

FILES_${PN}_append = "${systemd_unitdir}/system ${bindir}"

do_install_append() {
    install -d ${D}${systemd_unitdir}/system
    install -m 644 ${WORKDIR}/jody-w1-bt-init.service ${D}${systemd_unitdir}/system/


    install -d ${D}${bindir}
    install -m 744 ${WORKDIR}/jody-w1-bt ${D}${bindir}
}
