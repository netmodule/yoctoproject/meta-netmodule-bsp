DESCRIPTION = "USB-Hub Reset on HW23"
HOMEPAGE = "www.netmodule.com"
LICENSE = "MIT"
SECTION = "bsp"
RDEPENDS_${PN} = "usbutils coreutils"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit systemd

SRC_URI = " \
           file://usb-hub-reset.service \
           file://usb-hub-reset\
          "

S = "${WORKDIR}"

SYSTEMD_SERVICE_${PN} = " \
                          usb-hub-reset.service \
                        "

FILES_${PN} = "${systemd_unitdir}/system ${bindir}"

do_install() {
    install -d ${D}${systemd_unitdir}/system
    install -m 644 ${WORKDIR}/usb-hub-reset.service ${D}${systemd_unitdir}/system/

    install -d ${D}${bindir}
    install -m 744 ${WORKDIR}/usb-hub-reset ${D}${bindir}
}
