require recipes-bsp/u-boot/u-boot.inc

LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://Licenses/README;md5=a2c678cfd4a4d97135585cad908541c6"

S = "${WORKDIR}/git"
B = "${WORKDIR}/build"
do_configure[cleandirs] = "${B}"
UBOOT_INITIAL_ENV = ""

# This variable is set to "0" in distro in order to get reproducible build
# It is however not needed for uboot and unsetting it allows us to keep the build date in uboot output
unset SOURCE_DATE_EPOCH
