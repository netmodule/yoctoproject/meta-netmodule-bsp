require u-boot-nm.inc

# Be aware github/netmodule git
SRC_URI = "git://gitlab.com/netmodule/bootloader/netmodule-uboot.git;protocol=ssh;user=git;branch=2016.05/standard/am335x"

# Should be updated when a new U-Boot Version is available
SRCREV = "f3e914c320ba46b7df6d3b8b32cde9f319dc2449"

SPL_BINARY = "MLO"
UBOOT_SUFFIX = "img"

do_deploy() {
    # xmodem files
    cp ${B}/spl/u-boot-spl.bin ${DEPLOYDIR}/spl-u-boot-${MACHINE}.xmodem.bin
    cp ${B}/u-boot.bin ${DEPLOYDIR}/u-boot-${MACHINE}.xmodem.bin

    # files for wic file
    cp ${B}/MLO ${DEPLOYDIR}/
    cp ${B}/u-boot.img ${DEPLOYDIR}/

    # files for user usage
    cp ${B}/MLO ${DEPLOYDIR}/spl-u-boot-${MACHINE}.${UBOOT_SUFFIX}
    cp ${B}/u-boot.img ${DEPLOYDIR}/u-boot-${MACHINE}.${UBOOT_SUFFIX}
}
