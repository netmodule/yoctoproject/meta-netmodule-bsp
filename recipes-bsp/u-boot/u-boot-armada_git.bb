require u-boot-nm.inc

# Be aware github/netmodule git
SRC_URI = "git://gitlab.com/netmodule/bootloader/netmodule-uboot.git;protocol=ssh;user=git;branch=2017.11/standard/armada-385"

DEPENDS += "bc-native"

# Should be updated when a new U-Boot Version is available
#SRCREV ?= "68d28424cf41e141207d9d8af76c5bc5e01a55e2"
SRCREV ?= "68d28424cf41e141207d9d8af76c5bc5e01a55e2"

UBOOT_SUFFIX = "kwb"
UBOOT_BINARY = "u-boot-spl.${UBOOT_SUFFIX}"

do_deploy() {
    # xmodem files
    cp ${B}/spl/u-boot-spl.bin ${DEPLOYDIR}/spl-u-boot-${MACHINE}.xmodem.bin
    cp ${B}/u-boot.bin ${DEPLOYDIR}/u-boot-${MACHINE}.xmodem.bin

    # file for wic file
    cp ${B}/${UBOOT_BINARY} ${DEPLOYDIR}/u-boot-spl.${UBOOT_SUFFIX}

    # file for user usage
    cp ${B}/${UBOOT_BINARY} ${DEPLOYDIR}/u-boot-${MACHINE}.${UBOOT_SUFFIX}
}

