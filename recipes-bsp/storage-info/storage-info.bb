SUMMARY = "Storage Information Tool"
SECTION = "base"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=81438338854bd7a09cb97215f36db46b"

SRC_URI = "git://gitlab.com/netmodule/tools/storage-info.git;protocol=ssh;user=git;branch=develop"
SRCREV ?= "6584feb5fb3e18572fee1cea9e20a6b8e5cd0251"

PV = "0.0.1+git${SRCPV}"


DEPENDS_append = " libnmapp"

inherit systemd

S = "${WORKDIR}/git"
B = "${S}/build"

SYSTEMD_SERVICE_${PN} = "storage-info.service"

FILES_${PN} += " \
                  ${bindir}/storage-info \
                  ${systemd_system_unitdir}/storage-info.service \
                  ${sysconfdir}/storage/storage-info.conf \
               "

# build variables for the target (rest is default)
EXTRA_OEMAKE_append = " 'BUILD_TARGET=target' 'BUILD_CONFIG=rls' 'SYSROOT=${STAGING_DIR_TARGET}'"

do_install() {
    install -d ${D}${bindir}
    install -m 755 ${B}/rls/target/${BPN} ${D}${bindir}
    install -d ${D}${sysconfdir}/storage
    install -m 644 ${S}/config/${BPN}.conf ${D}${sysconfdir}/storage/
    install -d ${D}${systemd_system_unitdir}
    install -m 644 ${S}/systemd/${BPN}.service ${D}${systemd_system_unitdir}
}

