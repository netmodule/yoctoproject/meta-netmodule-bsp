DESCRIPTION = "Bootloader configuration tool"
LICENSE = "Proprietary"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/Proprietary;md5=0557f9d92cf58f2ccdd50f62f8ac0b28"

RDEPENDS_${PN} += " \
    python3-setuptools \
    "
DEPENDS = "python3-setuptools-git-version-native"

inherit setuptools3 gitpkgv systemd

# GITPKGVTAG is undefined when parsing and cannot be used for PV
PKGV = "${GITPKGVTAG}"
PV = "1.0-git${SRCPV}"

SRC_URI = "git://gitlab.com/netmodule/tools/bootloader-config.git;protocol=ssh;user=git;branch=master"
SRCREV ?= "60269b7bbb4076247607c7a59b2134c3a5369f07"
S = "${WORKDIR}/git"

# Likely required in the future
#PACKAGE_ARCH = "${MACHINE_ARCH}"
inherit allarch
#do_configure_prepend() {
#    cp ${S}/platforms/${MACHINE}/* ${S}/bootloader_config
#}

SYSTEMD_SERVICE_${PN} = "bootloader-config.service"

# do_install is done by setuptools
do_install_append() {
    install -d ${D}/${systemd_unitdir}/system/
    install -m 0644 ${S}/systemd/bootloader-config.service ${D}/${systemd_unitdir}/system/
}

