# Copyright (C) 2019 Ramon Moesching <ramon.moesching@netmodule.com>
# Released under the MIT license (see COPYING.MIT for the terms)

DESCRIPTION = "Murata v2x module firmware loader service"
HOMEPAGE = "www.netmodule.com"
LICENSE = "MIT"
SECTION = "bsp/firmware"
RDEPENDS_${PN} = "dfu-util usbutils coreutils"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit systemd

SRC_URI = " \ 
           file://v2x-ieee802.11p.service \
           file://v2x-fw-load \
           file://v2x_dummy.rom \
           file://v2x0.conf \
           file://v2x0-placeholderFw.conf \
          "

S = "${WORKDIR}"

SYSTEMD_SERVICE_${PN} = " \
                          v2x-ieee802.11p.service \
                        "

FILES_${PN} = "${systemd_unitdir}/system ${bindir} ${nonarch_base_libdir}/firmware/v2x ${sysconfdir} "

do_install() {
    install -d ${D}${systemd_unitdir}/system
    install -m 644 ${WORKDIR}/v2x-ieee802.11p.service ${D}${systemd_unitdir}/system/

    install -d ${D}${nonarch_base_libdir}/firmware/v2x
    install -m 644 ${WORKDIR}/v2x_dummy.rom ${D}${nonarch_base_libdir}/firmware/v2x/SECTON.packed_bin.rom

    install -d ${D}${sysconfdir}
    if [ ! -z "${V2X_ENABLE_FW_LOAD}" ]; then
        install -m 544 ${WORKDIR}/v2x0.conf ${D}${sysconfdir}
    else
        install -m 544 ${WORKDIR}/v2x0-placeholderFw.conf ${D}${sysconfdir}/v2x0.conf
    fi

    install -d ${D}${bindir}
    install -m 744 ${WORKDIR}/v2x-fw-load ${D}${bindir}
}
