SUMMARY = "Set mac addresses of Wifi and bluetooth chips"
DESCRIPTION = "Small scripts that set the mac addresses based on ethernet mac"
AUTHOR = "Alexandre Bard"

LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"
PR = "r2"

inherit systemd

SRC_URI =  " \
    file://wlan-address-set.service \
    file://bt-address-set.service \
    file://mac-address-set.sh \
    "

S = "${WORKDIR}"

INHIBIT_PACKAGE_DEBUG_SPLIT = "1"

SYSTEMD_SERVICE_${PN} = "wlan-address-set.service bt-address-set.service"
SYSTEMD_AUTO_ENABLE ?= "enable"

FILES_${PN}_append = " \
                    /lib \
                    /usr \
                    "

PACKAGE_ARCH = "${MACHINE_ARCH}"

do_install () {
    install -d ${D}${systemd_unitdir}/system/
    install -m 0644 wlan-address-set.service ${D}${systemd_unitdir}/system/
    install -m 0644 bt-address-set.service ${D}${systemd_unitdir}/system/

    # Invert BT mac for TI chips
    if ${@bb.utils.contains('MACHINE_FEATURES', 'tibluetooth', 'true', 'false',d)}; then
        sed -i 's/^ExecStart=.*/& -i/g' ${D}${systemd_unitdir}/system/bt-address-set.service
    fi

    install -d ${D}/usr/bin
    install -m 0755 mac-address-set.sh ${D}/usr/bin/mac-address-set
}

