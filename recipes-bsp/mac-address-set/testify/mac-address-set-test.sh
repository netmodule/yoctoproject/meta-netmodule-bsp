#!/usr/bin/env bash

source ../files/mac-address-set.sh testify
source ./testify/testify.bash

assert expect "$(log_error 'test')" "$0: error: test" "Test for Error logging" "should succeed"
assert expect "$(log_info 'test')" "$0: info: test" "Test for Info logging" "should succeed"

# Testing VCU1 mac address converter
assert expect "$(vcu1_eth_to_wlan '7C:97:63:50:00:00')" "7C:97:63:70:00:00" "Test VCU1 WLAN 1" "should match"
assert expect "$(vcu1_eth_to_wlan '7c:97:63:50:00:00')" "7C:97:63:70:00:00" "Test VCU1 WLAN 2" "should match"

assert expect "$(vcu1_eth_to_bt '7C:97:63:50:00:0A')" "7C:97:63:80:00:0A" "Test VCU1 BT 1" "should match"
assert expect "$(vcu1_eth_to_bt '7c:97:63:50:00:0a')" "7C:97:63:80:00:0A" "Test VCU1 BT 2" "should match"

# Testing VCU2 mac address converter
assert expect "$(vcu2_eth_to_wlan_and_bt 'wlan' '00:11:2b:aa:cd:00')" "00:11:2B:AA:CD:03" "Test VCU2 WLAN 1" "should match"
assert expect "$(vcu2_eth_to_wlan_and_bt 'wlan' '00:11:2B:AA:CD:00')" "00:11:2B:AA:CD:03" "Test VCU2 WLAN 2" "should match"
assert expect "$(vcu2_eth_to_wlan_and_bt 'wlan' '00:11:2b:00:00:39')" "00:11:2B:00:00:3C" "Test VCU2 WLAN 3" "should match"
assert expect "$(vcu2_eth_to_wlan_and_bt 'wlan' '00:11:2b:00:00:f0')" "00:11:2B:00:00:F3" "Test VCU2 WLAN 4" "should match"
assert expect "$(vcu2_eth_to_wlan_and_bt 'wlan' '00:11:2b:00:00:f6')" "00:11:2B:00:00:F9" "Test VCU2 WLAN 5" "should match"
assert expect "$(vcu2_eth_to_wlan_and_bt 'wlan' '00:11:2b:00:00:f7')" "00:11:2B:00:00:FA" "Test VCU2 WLAN 6" "should match"
#assert expect "$(vcu2_eth_to_wlan_and_bt 'wlan' '00:11:2b:00:00:RS')" "" "Test VCU2 WLAN 7" "should match"
#assert expect "$(vcu2_eth_to_wlan_and_bt 'wlan' '00:11:2b:00:00:FF')" "" "Test VCU2 WLAN 8" "should match"
assert expect "$(vcu2_eth_to_wlan_and_bt 'wlan' '00:11:2b:00:00:06')" "00:11:2B:00:00:09" "Test VCU2 WLAN 9" "should match"

assert expect "$(vcu2_eth_to_wlan_and_bt 'bt' '00:11:2b:aa:cd:00')" "00:11:2B:AA:CD:04" "Test VCU2 BT 1" "should match"
assert expect "$(vcu2_eth_to_wlan_and_bt 'bt' '00:11:2B:AA:CD:00')" "00:11:2B:AA:CD:04" "Test VCU2 BT 2" "should match"
assert expect "$(vcu2_eth_to_wlan_and_bt 'bt' '00:11:2b:00:00:39')" "00:11:2B:00:00:3D" "Test VCU2 BT 3" "should match"
assert expect "$(vcu2_eth_to_wlan_and_bt 'bt' '00:11:2b:00:00:f0')" "00:11:2B:00:00:F4" "Test VCU2 BT 4" "should match"
assert expect "$(vcu2_eth_to_wlan_and_bt 'bt' '00:11:2b:00:00:f6')" "00:11:2B:00:00:FA" "Test VCU2 BT 5" "should match"
assert expect "$(vcu2_eth_to_wlan_and_bt 'bt' '00:11:2b:00:00:f7')" "00:11:2B:00:00:FB" "Test VCU2 BT 6" "should match"
#assert expect "$(vcu2_eth_to_wlan_and_bt 'wlan' '00:11:2b:00:00:RS')" "" "Test VCU2 BT 7" "should match"
#assert expect "$(vcu2_eth_to_wlan_and_bt 'wlan' '00:11:2b:00:00:FF')" "" "Test VCU2 BT 8" "should match"
assert expect "$(vcu2_eth_to_wlan_and_bt 'bt' '00:11:2b:00:00:06')" "00:11:2B:00:00:0A" "Test VCU2 BT 9" "should match"

# Examples:
#assert expect "$(Name 'Jane' 'Doe')" "John Doe" "Test for Name Function" "should fail"
#assert expect "$(Name 'Jane' 'Doe')" "Jane Doe" "Test for Name Function" "should succeed"
#assert status "Name" "5" "Test for status code" "should return 5"
#assert status "Name 'Jane' 'Doe'" "0" "Test for Status Code" "should return 0"
#assert status "Name 'Jane' " "3" "Test for Status Code" "should return 3"
#assert status "Name 'Jane' 'Doe'" "12" "Test for Status Code" "it should fail"
#assert regex "$(Name 'Jane' 'Doe')" "Jane" "Test for Regexp" "it should match"
#assert regex "123victory" "\W" "Test for Regexp Non Word Character" "it should fail if match failes"
#assert expect "$((2+2))" "4" "Test for Simple Math Operation" "It should succeed"
#assert regex "What is the difference between 6 and half a dozen" "[[:digit:]]" "Match Number Regular Expression" "It should succeed"
#assert status "ls ." "0" "List in current dir" "it should return 0"

assert done

