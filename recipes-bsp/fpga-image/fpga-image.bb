DESCRIPTION = "Fpga images"
LICENSE = "Proprietary"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/Proprietary;md5=0557f9d92cf58f2ccdd50f62f8ac0b28"

inherit deploy

COMPATIBLE_MACHINE = "(armada-385-hw17|armada-385-nrhw18)"
SRC_URI = "file://LG00000000"

PACKAGE_ARCH = "${MACHINE_ARCH}"

PV = "1.0.0"


do_install () {
    install -d ${D}/logic
    install -m 0644 ${WORKDIR}/LG00000000 ${D}/logic/
}

do_deploy () {
    cp ${WORKDIR}/LG00000000 ${DEPLOYDIR}/fpga-image-${MACHINE}
}

FILES_${PN} += "/logic/LG00000000" 

addtask deploy before do_build after do_compile
